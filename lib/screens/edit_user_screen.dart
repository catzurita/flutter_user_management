import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_user_management/widgets/snackbar.dart';
import '../models/user.dart';

import '../utils/api.dart';

class EditUserScreen extends StatefulWidget {
  
  final User user;

  EditUserScreen(this.user);

  @override
  _EditUserScreenState createState() => _EditUserScreenState();
}

class _EditUserScreenState extends State<EditUserScreen> {
    
    Future? _futureEditUser;

    final _formKey = GlobalKey<FormState>();
    final _tffNameController = TextEditingController();
    final _tffEmailController = TextEditingController();
    final _tffPhoneController = TextEditingController();
    final _tffWebsiteController = TextEditingController();
    
    void editUser(BuildContext context){

        setState(() {
            _futureEditUser = API().editUser(
                name: _tffNameController.text,
                email: _tffEmailController.text,
                phone: _tffPhoneController.text,
                website: _tffWebsiteController.text,
                id: widget.user.id
            ).catchError((error){
                showSnackBar(context, error.message);
            });
        });
    }

    @override
    Widget build(BuildContext context) {

        Widget tffName = TextFormField(
            decoration: InputDecoration(labelText: 'Name'),
            controller: _tffNameController,
        );

        Widget tffEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
        );

        Widget tffPhone = TextFormField(
            decoration: InputDecoration(labelText: 'Phone No.'),
            controller: _tffPhoneController,
            
        );

        Widget tffWebsite = TextFormField(
            decoration: InputDecoration(labelText: 'Website'),
            controller: _tffWebsiteController,
            
        );

        Widget btnEditUser = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Edit User'),
                onPressed: (){
                    print(widget.user);
                    if(_formKey.currentState!.validate()){
                        editUser(context);      
                    } else {
                        print('The form is not valid');
                    }
                }
            )
        );

        Widget formEditUser = Form(
            key: _formKey,
            child: Column(
                children: [
                    tffName,
                    tffEmail,
                    tffPhone,
                    tffWebsite,
                    btnEditUser
                ],
            )
        );

        Widget editUserFB = FutureBuilder(
            future: _futureEditUser,
            builder: (context,snapshot){
                if(_futureEditUser == null){
                    return formEditUser;
                } else if (snapshot.connectionState == ConnectionState.done){
                    if(snapshot.hasError){
                        return formEditUser;
                    } else {
                        Timer(Duration(milliseconds: 1), () async{
                            print(snapshot);
                            showSnackBar(context, "Edit of user successful");
                            Navigator.pushReplacementNamed(context,'/');
                        });
                        return Container();
                    }
                } else {
                    return  Center(
                        child: CircularProgressIndicator()
                    );
                }
            }
        );

        return Scaffold(
            appBar: AppBar(
                title: Text('Edit User')
            ),
            body: SingleChildScrollView(
                child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(16),
                    child: editUserFB,
                )
            ),
        );
    }

}