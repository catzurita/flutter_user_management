import 'dart:async';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter_user_management/widgets/snackbar.dart';

import '../utils/api.dart';

class AddUserScreen extends StatefulWidget {
  const AddUserScreen({ Key? key }) : super(key: key);

  @override
  _AddUserScreenState createState() => _AddUserScreenState();
}

class _AddUserScreenState extends State<AddUserScreen> {
    
    Future? _futureAddUser;

    final _formKey = GlobalKey<FormState>();
    final _tffNameController = TextEditingController();
    final _tffEmailController = TextEditingController();
    final _tffPhoneController = TextEditingController();
    final _tffWebsiteController = TextEditingController();

    void addUser(BuildContext context){

        setState(() {
            _futureAddUser = API().addUser(
                name: _tffNameController.text,
                email: _tffEmailController.text,
                phone: _tffPhoneController.text,
                website: _tffWebsiteController.text
            ).catchError((error){
                showSnackBar(context, error.message);
            });
        });
    }

    @override
    Widget build(BuildContext context) {

        Widget tffName = TextFormField(
            decoration: InputDecoration(labelText: 'Name'),
            controller: _tffNameController,
            validator: (name){
                if(name == null || name.isEmpty){
                    return 'The name must be provided';
                } else { 
                    return null;
                }
            },
        );

        Widget tffEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            validator: (email){
                if(email == null || email.isEmpty){
                    return 'The email must be provided';
                } else if (EmailValidator.validate(email) == false){
                    return 'A valid email must be provided';
                } else { 
                    return null;
                }
            },
        );

        Widget tffPhone = TextFormField(
            decoration: InputDecoration(labelText: 'Phone No.'),
            controller: _tffPhoneController,
            validator: (phone){
                if(phone == null || phone.isEmpty){
                    return 'The Phone No. must be provided';
                } else { 
                    return null;
                }
            },
        );

        Widget tffWebsite = TextFormField(
            decoration: InputDecoration(labelText: 'Website'),
            controller: _tffWebsiteController,
            validator: (web){
                if(web == null || web.isEmpty){
                    return 'The Phone No. must be provided';
                } else { 
                    return null;
                }
            },
        );

        Widget btnAddUser = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Add User'),
                onPressed: (){
                    if(_formKey.currentState!.validate()){
                        addUser(context);      
                    } else {
                        print('The form is not valid');
                    }
                }
            )
        );

        Widget formAddUser = Form(
            key: _formKey,
            child: Column(
                children: [
                    tffName,
                    tffEmail,
                    tffPhone,
                    tffWebsite,
                    btnAddUser
                ],
            )
        );

        Widget addUserFB = FutureBuilder(
            future: _futureAddUser,
            builder: (context,snapshot){
                if(_futureAddUser == null){
                    return formAddUser;
                } else if (snapshot.connectionState == ConnectionState.done){
                    if(snapshot.hasError){
                        return formAddUser;
                    } else {
                        Timer(Duration(milliseconds: 1), () async{
                            print(snapshot.data);
                            showSnackBar(context, "Addition of user successful");
                            Navigator.pushReplacementNamed(context,'/');
                        });
                        return Container();
                    }
                } else {
                    return  Center(
                        child: CircularProgressIndicator()
                    );
                }
            }
        );

        return Scaffold(
            appBar: AppBar(
                title: Text('Add User')
            ),
            body: SingleChildScrollView(
                child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(16),
                    child: addUserFB,
                )
            ),
        );
    }

}