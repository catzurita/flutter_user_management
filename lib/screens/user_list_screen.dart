import 'package:flutter/material.dart';
import 'package:flutter_user_management/screens/edit_user_screen.dart';

import '/utils/api.dart';
import '/models/user.dart';
import '../screens/user_list_screen.dart';

class UserListScreen extends StatefulWidget {
	@override
	UserListScreenState createState() => UserListScreenState();
}

class UserListScreenState extends State<UserListScreen> {
    Future? futureUsers;
    final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

    List<Widget> generateListTiles(List<User> users){
        List<Widget> listTiles = [];

        for (User user in users){
            listTiles.add(
                ListTile(
                    title: Text(user.name),
                    subtitle: Text(user.email),
                    trailing: IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () async{
                            await Navigator.push(context, 
                                MaterialPageRoute(
                                 builder: (context) => EditUserScreen(user)
                                )
                            ); 
                        }
                            
                    )
                )
            );
        }

        return listTiles;
    }
    @override 
    void initState(){
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            // List<User> users = await API().getUsers();
            setState(() {
              futureUsers = API().getUsers();
            });
            // If future na ginamit, kahit wala na async await, dahil may nagaantay na ng response from a request which is si future na 
            // print(users);
        });
    }
	@override
	Widget build(BuildContext context) {
        Widget fbUserList = FutureBuilder(
            future: futureUsers,
            builder: (context, snapshot){
                if (snapshot.connectionState == ConnectionState.done){
                    if (snapshot.hasError){
                        return Center(
                            child: Text('Could not load the user ')
                        );
                    } 
                    return RefreshIndicator(
                        key: refreshIndicatorKey,
                        onRefresh: () async {
                            setState(() {
                                futureUsers = API().getUsers();
                            });
                        },
                        child: ListView(
                            padding: EdgeInsets.all(8),
                            children: generateListTiles(snapshot.data as List<User>)
                        )
                    );
                }
                return Center(
                    child: CircularProgressIndicator()
                );
            }
        );

		return Scaffold(
			appBar: AppBar(
                title: Text('User List')
            ),
            drawer: Drawer(
                child: ListTile(
                    title: Text('Add User'),
                    onTap: () async {
                        Navigator.pushNamed(context,  '/addUser');
                    }
                )
            ),
			body: Container(
                child: fbUserList,
            )
		);
	}
}