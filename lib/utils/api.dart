import 'package:dio/dio.dart';

import '/models/user.dart';

class API {
     String url = 'https://jsonplaceholder.typicode.com/users';

	Future getUsers() async {
		try {
			var response = await Dio().get(url);
			List<User> users = [];

			for (var user in response.data) {
				// The 'user' here is of Map<String, dynamic> type.
				// The 'user' map is converted into a User object.
				// After conversion, the object is added to the list.
				users.add(User.fromJson(user));
			}
			return users;
		} catch (exception) {
			throw exception;
		}
	}

    Future addUser({
        required String name,
        required String email,
        required String phone,
        required String website
    }) async {
        try{
            
            var formData = {
                'name': name,
                'email': email,
                'phone': phone,
                'website': website
            };
            
            var response = await Dio().post(
                url,
                data: formData
            );

            print(response.data);
            return(response.data);
            // if(response.statusCode == 200){
            //     return response.data;
            // } else {
            //     throw Exception("Addition of user failed");
            // }
        } catch (exception){
            throw exception;
        }
    }

    Future editUser({
        required String name,
        required String email,
        required String phone,
        required String website,
        required int id
    }) async {
        try{
            var formData = {
                'name': name,
                'email': email,
                'phone': phone,
                'website': website
            };

            var response = await Dio().put(
                url + '/$id',
                data: formData
            );
            print(response.data);
            return(response.data);
            // if(response.statusCode == 200){
            //      return response.data;
            // } else {
            //     throw Exception("Edit of user failed");
            // }
        } catch (exception){
            throw exception;
        }
    }
}
